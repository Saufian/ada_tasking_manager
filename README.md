# Ada Tasking Manager

Ada Tasking Manager is a light project offering, through the use of a generic
package, the possibility to perform time consuming operations on data on a
specific number of threads.

![Tasking_Manager concept in image](assets/images/tasking_manager_concept.png)

This Tasking Manager will manage the execution of a function on each input
element, by distributing the load on several threads (number is set by the
user), then provide an array containing the results.

## Quick start

### Download

The source code can be download from [Gitlab](https://gitlab.com/Saufian/ada_tasking_manager/-/archive/master/ada_tasking_manager-master.zip).

Additionnaly, you can clone this project:

```bash
git clone https://gitlab.com/Saufian/ada_tasking_manager.git
```

### Build

#### Build library

Building the project as a library does not work for now (will be corrected in a
future update).

You can still use this project by adding the files in `./src/` to your current
code base. You don't need to use any special compiler options to compile these
files.

#### Build example

You can build the provided example by using gprbuild on the
[tasking_example.gpr](tasking_example.gpr) file. You will then find an executable in the newly created `./bin/` folder.

```bash
# Compiling
gprbuild -p tasking_example.gpr
# Executing
./bin/example
```

This example use a time consuming function to invert the case of the array "Hello World!". It's done multiple time, with different number of thread.

### Run the `Tasking_Manager`

Instantiate the "Tasking_Manager" library in your project and define the types
you want to use as follow:

- `Task_Input_T` is the type of data you want to process
- `Task_Output_T` being the type you want to obtain at the end of the process
- `Array_Range_T` which is the type used as range in your data array
- And lastly the `Task_Function`, the function that will transform each input
  element into an output element (with `Task_Input_T` as the only input and
  returning a `Task_Output_T` element)

Then you just have to provide to the newly created instance (via a call to the
`Launch_Task` function) a list of input data (corresponding to an array of
`aliased Task_Input_T` of index `Array_Range_T`) and the number of threads to
use. This package uses the given function on each input data. The output, in the
form of a array of `Task_Output_T` of index `Array_Range_T`, can be obtained by
calling the `Get_Task_Result` function.

Notes:

- The original order of the input data array is kept in the output array.
- This package does not manage in any case the concurrent data access issues
  that can append when calling concurrently the provided function: the user has
  to take into account this issue in the creation of the function.
- There is no certainty that the execution on each element will be done in the
  order of the data array.
- The call of the `Get_Task_Result` function is blocking (you have to wait for
  all tasks to end if it is not done already)

See [example code](example/example.adb) to get an idea of how to implement this
library.

## How does it work?

This project sets up a thread manager that will coordinate the work of N workers
(with N corresponding to the number of threads to use). Each worker will execute
the function indicated at the instantiation time, with a specific parameter
(picked up from the input list of `Task_Input_T` element) as input and will
return an element of type `Task_Output_T` for the output. These output are
collected by the manager, and can be consulted anytime after the end of all
workers by calling `Get_Task_Result`.

## Roadmap

Future updates will focus on the following:

- Raise error from the tasking manager and re-raise error raised by the
  provided function
- Allow the use of protected type instead of the current function
- Publication of library efficiency measures.
- Fix library creation
- Add more inline documentation

## License

This project is under the MIT License (see the [LICENSE](LICENSE) file), which
means that you can do whatever you want with this project, as long as the
copyright notice and the permission notice remain. This project is provided
without warranty of any kind.

## Contact

You can contact the developer by opening an issue in the current repo.
