-- Copyright (c) 2023 Thomas Bailleux
--
-- Licensed under the MIT license: https://opensource.org/licenses/MIT
-- Permission is granted to use, copy, modify, and redistribute the work.
-- Full license information available in the project LICENSE file.

package body Tasking_Manager is

  procedure Launch_Task (Data_In : access Task_Input_Array_T; Thread_Number : in Positive) is
    Worker_Array :  array (Positive'First..Thread_Number) of Thread_Worker(Thread_Manager'access);
  begin
    Thread_Manager.Setup_Manager(Data_In);
    for Worker of Worker_Array loop
      Worker.Start_Working;
    end loop;
  end Launch_Task;


  procedure Get_Task_Result (Data_Out : out Task_Output_Array_T) is
  begin
    Thread_Manager.Ask_Result(Data_Out);
  end Get_Task_Result;


  protected body Thread_Manager_Type is
    procedure Setup_Manager (Data_In : access Task_Input_Array_T) is
    begin
      if Is_Initialized then null;  -- TODO : raise an error if the manager is already working
      end if;
      Is_Content_Exhausted := (if Data_In'Length = 0 then True else False);
      Content_Array := Data_In;
      Array_Cursor  := Array_Range_T'First;
      Result_Array  := (others=> <>);
      Worker_Number := 0;
      Is_Free := True;
      Is_Initialized := True;
    end Setup_Manager;

    entry Begin_Work (Rate_Elem : out Rate_Array_Type; Continue : out Boolean)
      when Is_Initialized and Is_Free is  -- TODO: Check if "Is_Free" is needed
    begin
      Is_Free := False;
      Get_Next(Rate_Elem, Continue);
      Is_Free := True;
    end Begin_Work;

    entry Next_Work (Rate_Elem : in out Rate_Array_Type; Continue : out Boolean)
      when Is_Initialized and Is_Free is
    begin
      Is_Free := False;
      Worker_Number := Worker_Number - 1;
      Result_Array(Rate_Elem.Pos) := Rate_Elem.Rate_Output;
      Get_Next(Rate_Elem, Continue);
      Is_Free := True;
    end Next_Work;

    procedure Get_Next (Rate_Elem : out Rate_Array_Type; Continue : out Boolean) is
    begin
      if not Is_Content_Exhausted then
        Continue := True;
        Rate_Elem.Pos := Array_Cursor;
        Rate_Elem.Input_P := Content_Array.all(Array_Cursor)'Unchecked_Access;
        Rate_Elem.Rate_Output := Null_Rate_Array.Rate_Output;  -- default value
        Worker_Number := Worker_Number + 1;
        if Array_Cursor = Array_Range_T'Last then
          Is_Content_Exhausted := True;
        else
          Array_Cursor := Array_Range_T'Succ(Array_Cursor);
        end if;
      else
        Continue := False;
        Rate_Elem := Null_Rate_Array;
      end if;
    end Get_Next;

    entry Ask_Result (Data_Out : out Task_Output_Array_T)
      when Is_Content_Exhausted and Worker_Number = 0 is
    begin
      Is_Initialized := False;-- work done, we can unintialize
      Data_Out := Result_Array;
    end Ask_Result;
  end Thread_Manager_Type;


  task body Thread_Worker is
    Working_Data : Rate_Array_Type;
    Continue : Boolean;
  begin
    select
      accept Start_Working do
        null;  -- no synchronized event
      end Start_Working;

        Manager.Begin_Work(Working_Data, Continue);
        while Continue loop
          -- possibly time-consuming
          Working_Data.Rate_Output := Task_Function(Working_Data.Input_P.all);  -- TODO : re-raise potential error
          Manager.Next_Work(Working_Data, Continue);
        end loop;
    or
      terminate;
    end select;
  end Thread_Worker;
end Tasking_Manager;
