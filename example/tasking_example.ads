-- Copyright (c) 2023 Thomas Bailleux
--
-- Licensed under the MIT license: https://opensource.org/licenses/MIT
-- Permission is granted to use, copy, modify, and redistribute the work.
-- Full license information available in the project LICENSE file.

package Tasking_Example is
  type Task_Input_Type  is new Character;

  type Task_Result_Type is new Character;

  function Tasking_Function(Elem : in Task_Input_Type) return Task_Result_Type;

  procedure Print_Result(Elem : in Task_Result_Type);
end Tasking_Example;