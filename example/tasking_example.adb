-- Copyright (c) 2023 Thomas Bailleux
--
-- Licensed under the MIT license: https://opensource.org/licenses/MIT
-- Permission is granted to use, copy, modify, and redistribute the work.
-- Full license information available in the project LICENSE file.

with Ada.Text_IO;
with Ada.Real_Time; use Ada.Real_Time;

package body Tasking_Example is

  function Tasking_Function(Elem : in Task_Input_Type) return Task_Result_Type is
    Lower_To_Upper : constant array(Task_Input_Type range 'a'..'z') of Task_Result_Type := "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    Upper_To_Lower : constant array(Task_Input_Type range 'A'..'Z') of Task_Result_Type := "abcdefghijklmnopqrstuvwxyz";
  begin
    -- simulate CPU and time consuming operation (5 seconds)
    declare
      Operation : Boolean := true;
      Time_Consumed: Time_Span := Seconds(5);
      Stop_Time    : Time      := Clock + Time_Consumed;
    begin
      while Clock < Stop_Time loop
        Operation := not Operation;
      end loop;
    end;

    if Elem in Lower_To_Upper'range then
      return Lower_To_Upper(Elem);
    else
      if Elem in Upper_To_Lower'range then
        return Upper_To_Lower(Elem);
      else return Task_Result_Type(Elem);
      end if;
    end if;
  end Tasking_Function;


  procedure Print_Result(Elem : in Task_Result_Type) is
  begin
    Ada.Text_IO.Put(Task_Result_Type'Image(Elem)(2));
  end Print_Result;

end Tasking_Example;