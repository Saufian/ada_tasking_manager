-- Copyright (c) 2023 Thomas Bailleux
--
-- Licensed under the MIT license: https://opensource.org/licenses/MIT
-- Permission is granted to use, copy, modify, and redistribute the work.
-- Full license information available in the project LICENSE file.

with Tasking_Example; use Tasking_Example;
with Tasking_Manager;

with Ada.Text_IO;


procedure Example is
  type Test_Array_Range is range 1..12;
  package Tasking_Test is new Tasking_Manager (
    Task_Input_T => Task_Input_Type,
    Task_Output_T => Task_Result_Type,
    Array_Range_T => (Test_Array_Range),
    Task_Function => Tasking_Function);
  
  Test_Var     : aliased Tasking_Test.Task_Input_Array_T:= "Hello World!";
  Solution_Var : Tasking_Test.Task_Output_Array_T;

  procedure Use_Manager (Thread_Number: in Natural) is
    begin
      Tasking_Test.Launch_Task(Test_Var'Access, Thread_Number);
      Tasking_Test.Get_Task_Result(Solution_Var);
      Ada.Text_IO.Put_Line("");

      for Elem of Solution_Var loop
        Print_Result(Elem);  -- Printing
      end loop;
      Ada.Text_IO.Put_Line("");
    end Use_Manager;

begin
--  -- Launch the time-consuming transformation on each element (1 thread)
--  for Index in Test_Array_Range loop
--    Solution_Var(Index) := Tasking_Function(Test_Var(Index));
--  end loop;
--  Ada.Text_IO.Put_Line("");
--
--  for Elem of Solution_Var loop
--    Print_Result(Elem);  -- Printing
--  end loop;
--  Ada.Text_IO.Put_Line("");

  -- TODO : Use basic tasking as a comparaison
--  for Elem of Solution_Var loop
--    Print_Result(Elem);  -- Printing
--  end loop;
--  Ada.Text_IO.Put_Line("");

  -- Use of the tasking manager with N thread.
  Ada.Text_IO.Put_Line("1:");
  Use_Manager(1);
  Ada.Text_IO.Put_Line("2:");
  Use_Manager(2);
  Ada.Text_IO.Put_Line("3:");
  Use_Manager(3);
  Ada.Text_IO.Put_Line("4:");
  Use_Manager(4);
end Example;